package com.example.androtvswim

import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var currMain : Drawable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        currMain = getDrawable(R.drawable.katana_010)!!
    }

    fun onClicked(view: View) {
        val temp = currMain
        image_main.setImageDrawable((view as ImageView).drawable)
        currMain = view.drawable
        view.setImageDrawable(temp)
    }
}
